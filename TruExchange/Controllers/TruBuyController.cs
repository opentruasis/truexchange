﻿using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Newtonsoft.Json;
using TruExchange.Database;
using Microsoft.EntityFrameworkCore;
using TruExchange.Models;
using TruExchange.Enums;

namespace TruExchange.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class TruBuyController : Controller
    {
        // GET: api/<TruPriceController>
        [HttpGet]
        public ActionResult Get()
        {
            try
            {
                using (var db = TruExchangeDb.Instance.Db)
                {
                    var prices = db.TruBuyOrders.OrderBy(b => b.ID).ToList();

                    return Content(JsonConvert.SerializeObject(prices));
                }
            }
            catch (Exception e)
            {
                Response.StatusCode = 400;
                return Content(e.Message);
            }
        }

        // GET api/<TruPriceController>/5
        [HttpGet("{id}")]
        public async Task<ActionResult> Get(int id)
        {
            try
            {
                using (var db = TruExchangeDb.Instance.Db)
                {
                    var offer = await db.TruBuyOrders.FirstOrDefaultAsync(q => q.ID == id);
                    if (offer != null)
                    {
                        return Content(JsonConvert.SerializeObject(offer));
                    }
                    else
                    {
                        Response.StatusCode = 400;
                        return Content("No Such Entry");
                    }
                }
            }
            catch (Exception e)
            {
                Response.StatusCode = 400;
                return Content(e.Message);
            }
        }

        // POST api/<TruPriceController>
        [HttpPost]
        public async Task<ActionResult> Post(TruBuyOrder order)
        {
        

            var newLimitBuy = new TruBuyOrder()
            {
                Qty = order.Qty,
                LeavesQty = order.Qty,
                CumQty = 0,
                BuyerTruID = order.BuyerTruID,
                BuyerPubKey = order.BuyerPubKey,
                PaypalBuyerEmail = order.PaypalBuyerEmail,
                LimitPrice = order.LimitPrice,
                CreatedOn = DateTime.Now,
                UpdatedOn = DateTime.Now,
                OrderStatus = Enums.OrderStatus.Initiated
            };
            Console.WriteLine("TRU BUY ORDER POSTED!");
            Console.WriteLine(newLimitBuy);
            //try
            //{
            //   await tradingEngine.ProcessNewOrder(newLimitBuy);
            //}catch(Exception e)
            //{
            //    Console.WriteLine("ProcessNewOrder Error");
            //    Console.WriteLine(e.Message);
            //}
 
            using (var db = TruExchangeDb.Instance.Db)
            {
                await db.TruBuyOrders.AddAsync(newLimitBuy);
                await db.SaveChangesAsync();
 
                double fee = 5.0;
                var buyPrice = newLimitBuy.Qty * newLimitBuy.LimitPrice + fee;

                PaypalClient paypalClient = new PaypalClient();
                paypalClient.Initialize();
                var result = await paypalClient.GetPaypalPaymentIntent(buyPrice, newLimitBuy.ID.ToString());
 
                if (result != null)
                {
                    newLimitBuy.PaypalBuyURL = result.links[1].href;
                    newLimitBuy.PaypalTx = result.id;
                    await db.SaveChangesAsync();
 
                    return Content(JsonConvert.SerializeObject(newLimitBuy));
                }
                else
                {
                    Response.StatusCode = 500;
                    return Content(JsonConvert.SerializeObject(newLimitBuy));
                }

           



               
            }
        }

    

        // PUT api/<TruPriceController>/5
        [HttpPut("{id}")]
        public async Task<ActionResult> Put(int id, [FromBody] dynamic value)
        {
            try
            {
                using (var db = TruExchangeDb.Instance.Db)
                {
                    var offer = await db.TruBuyOrders.FirstOrDefaultAsync(q => q.ID == id);
                    if (offer != null)
                    {
                        var json = value.GetRawText();
                        var updateObj = JsonConvert.DeserializeObject<dynamic>(json);

                        if (updateObj.PaypalTx != null) offer.PaypalTx = updateObj.PaypalTx;
                        if (updateObj.OrderStatus != null) offer.OrderStatus = updateObj.OrderStatus;

                        offer.UpdatedOn = DateTime.Now;
                        Console.WriteLine("Updated TRU BUY... ");
                        Console.WriteLine(offer);
                        await db.SaveChangesAsync();

                        return Content(JsonConvert.SerializeObject(offer));
                    }
                    else
                    {
                        Response.StatusCode = 400;
                        return Content("No Such Entry");
                    }
                }
            }
            catch (Exception e)
            {
                Response.StatusCode = 400;
                return Content(e.Message);
            }

        }

        // DELETE api/<TruPriceController>/5
        [HttpDelete("{id}")]
        public async Task<ActionResult> Delete(int id)
        {
            try
            {
                using (var db = TruExchangeDb.Instance.Db)
                {
                    var offer = await db.TruBuyOrders.FirstOrDefaultAsync(q => q.ID == id);
                    if (offer != null)
                    {
                        if (offer.OrderStatus == OrderStatus.Paid)
                        {
                            double txFee = 5;
                            //do stuff.
                            var returnMoney = offer.LeavesQty * offer.LimitPrice - txFee;

                            if (offer.PaypalBuyerEmail != null)
                            {
                                PaypalClient paypalClient = new PaypalClient();
                                var returnMoneyEmail = offer.PaypalBuyerEmail;

                                _ = Task.Run(async () =>
                                { //Refund TRU BUY
                                var paypalResponse = await paypalClient.SendDollarsTo(returnMoneyEmail, returnMoney, offer.ID);
                                });
                            }
                            else
                            {
                                Console.WriteLine("SOMETHING WENT WRONG>>:(");
                                db.Remove(offer);
                                await db.SaveChangesAsync();
                            }
                        }
                        else
                        {
                            db.Remove(offer);
                            await db.SaveChangesAsync();
                        }
                        return Content(JsonConvert.SerializeObject(offer));
                    }
                    else
                    {
                        Response.StatusCode = 201;
                        return Content(JsonConvert.SerializeObject(new TruBuyOrder() { ID = id }));
                    }
                }
            }
            catch (Exception e)
            {
                Response.StatusCode = 400;
                return Content(e.Message);
            }
 
        }
 
    }
}
