﻿using Microsoft.AspNetCore.Mvc;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using TruExchange.Database;

// For more information on enabling Web API for empty projects, visit https://go.microsoft.com/fwlink/?LinkID=397860

namespace TruExchange.Models
{
    [Route("api/[controller]")]
    [ApiController]
    public class TruPriceController : ControllerBase
    {
        // GET: api/<TruPriceController>
        [HttpGet]
        public ActionResult GetPrices()
        {
            try
            {
                using (var db = TruExchangeDb.Instance.Db)
                {
                    var prices = db.TruPrices.OrderBy(b => b.ID).ToList();

                    return Content(JsonConvert.SerializeObject(prices));
                }
            }
            catch (Exception e)
            {
                Response.StatusCode = 400;
                return Content(e.Message);
            }
        }

        // GET api/<TruPriceController>/5
        [HttpGet("last")]
        public ActionResult GeLast(int id)
        {
            try
            {
                using (var db = TruExchangeDb.Instance.Db)
                {
                    var prices = db.TruPrices.OrderBy(b => b.ID).ToList();
                    if (prices.Count > 0)
                    {
                        return Content(JsonConvert.SerializeObject(prices[prices.Count - 1]));
                    }
                    else
                    {
                        return Content(JsonConvert.SerializeObject(null));
                    }
                }
            }
            catch (Exception e)
            {
                Response.StatusCode = 400;
                return Content(e.Message);
            }
        }

        
    }
}
