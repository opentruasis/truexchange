﻿using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using TruExchange.Database;

namespace TruExchange.Controllers
{
    [Route("[controller]")]
    [ApiController]
    public class PaypalBuyOrderCompleteController : Controller
    {
        [HttpGet]
        public async Task<IActionResult> Index()
        {
            string token = HttpContext.Request.Query["token"].ToString();
            Console.WriteLine("PAYPAL REDIRECT!");
            Console.WriteLine(token);
            Console.WriteLine("ARE WE GETTTING HERE???");


            //First Call Paypal and see if the order went through..
            if (token != "")
            {

                PaypalClient paypalclient = new PaypalClient();
                paypalclient.Initialize();
                var paymentResponse = await paypalclient.CheckOnPayment(token);
                if (paymentResponse != null)
                {
                    try
                    {
                        using (var db = TruExchangeDb.Instance.Db)
                        {

                            var buyOrder = db.TruBuyOrders.Where(order => order.PaypalTx == token).Single();
                            if (buyOrder != null)
                            {
                                if (paymentResponse.status == "APPROVED" || paymentResponse.status == "COMPLETED")
                                {
                                    buyOrder.OrderStatus = Enums.OrderStatus.Paid;
                                    buyOrder.PaypalBuyerEmail = paymentResponse.payer.email_address;
                                  
                                    await db.SaveChangesAsync();

                                    var processedOrder = await TruTradingEngine.Instance.ProcessNewOrder(buyOrder.ID, buyOrder.OrderType);
                                    Console.WriteLine("ProcessedORDER? @ Paypal Buy Orer Complete Page:)");
                                    Console.WriteLine(processedOrder);

                                    return View("Index");
                                }
                                else
                                {
                                    return View("NoPaidOrder");
                                }                                
                            }
                            else
                            {
                                return View("NoPaymentOnFile");
                            }
 
                        }
                    }
                    catch (Exception e)
                    {
                        return View("NoPaymentOnFile");
                    }
                }
                
            }
           

            return View("NoPaymentOnFile");
         
        }

    }
}
