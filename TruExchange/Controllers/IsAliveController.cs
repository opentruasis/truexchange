﻿using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace TruExchange.Controllers
{

    [Route("api/[controller]")]
    [ApiController]
    public class IsAliveController : Controller
    {

        [HttpGet]
        public String Index()
        {
            return "Yes it is alive";
        }
    }
}
