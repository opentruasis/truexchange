﻿using Microsoft.AspNetCore.Mvc;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using TruExchange.Database;
using TruExchange.Enums;
using TruExchange.Models;

namespace TruExchange.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class TruOrdersController : Controller
    {
        // GET: api/<TruPriceController>
        [HttpGet("bestbid")]
        public ActionResult GetBestBid()
        {
            //ORDER BOOK!
            List<TruSellOrder> Asks = new List<TruSellOrder>();
            List<TruBuyOrder> Bids = new List<TruBuyOrder>();
            using (var db = TruExchangeDb.Instance.Db)
            {
                try
                {
                    Bids = db.TruBuyOrders.Where(o => o.OrderStatus == OrderStatus.Paid).ToList();
                }
                catch (Exception e)
                {
                    Response.StatusCode = 400;
                    return Content(e.Message);
                }

                bool isABid = Bids.Count > 0;
                TruBuyOrder bestBid = null;
                if (isABid)
                {

                    Bids.Sort((a, b) =>
                    {
                        return a.LimitPrice >= b.LimitPrice ? 1 : -1;
                    });

                    bestBid = Bids[0]; // Maximum LimitPrices
                    return Content(JsonConvert.SerializeObject(bestBid));
                }
                else
                {
                    Response.StatusCode = 400;
                    return Content("No Available Bids");
                }
            }



        }

 
        [HttpGet("bestask")]
        public ActionResult GetBestAsk()
        {
            List<TruSellOrder> Asks = new List<TruSellOrder>();
            List<TruBuyOrder> Bids = new List<TruBuyOrder>();
            using (var db = TruExchangeDb.Instance.Db)
            {
                try
                {

                    Asks = db.TruSellOrders.Where(o => o.OrderStatus == OrderStatus.Paid).ToList();

                }
                catch (Exception e)
                {
                    Response.StatusCode = 400;
                    return Content(e.Message);
                }


                bool isAnAsk = Asks.Count > 0;
                TruSellOrder bestAsk = null;
                if (isAnAsk)
                {

                    Asks.Sort((a, b) =>
                    {
                        return a.LimitPrice <= b.LimitPrice ? -1 : 1;
                    });

                    bestAsk = Asks[0]; // Minimum LimitPrices

                    return Content(JsonConvert.SerializeObject(bestAsk));
                }
                else
                {
                    Response.StatusCode = 400;
                    return Content("No Available Asks");
                }

            }
        }
         
    }
}
