﻿using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Net.Sockets;
using System.Threading.Tasks;
using TruExchange.Database;
using TruExchange.Enums;
using TruExchange.Models;
using TruShared;
using TruShared.WebClient;
using TruV2Shared;

namespace TruExchange.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class TruSellController : Controller
    {
          // GET: api/<TruPriceController>
        [HttpGet]
        public ActionResult Get()
        {
            try
            {
                using (var db = TruExchangeDb.Instance.Db)
                {
                    var prices = db.TruSellOrders.OrderBy(b => b.ID).ToList();

                    return Content(JsonConvert.SerializeObject(prices));
                }
            }
            catch (Exception e)
            {
                Response.StatusCode = 400;
                return Content(e.Message);
            }
        }

        // GET api/<TruPriceController>/5
        [HttpGet("{id}")]
        public async Task<ActionResult> Get(int id)
        {
            try
            {
                using (var db = TruExchangeDb.Instance.Db)
                {
                    var offer = await db.TruSellOrders.FirstOrDefaultAsync(q => q.ID == id);
                    if (offer != null)
                    {
                        return Content(JsonConvert.SerializeObject(offer));
                    }
                    else
                    {
                        Response.StatusCode = 400;
                        return Content("No Such Entry");
                    }
                }
            }
            catch (Exception e)
            {
                Response.StatusCode = 400;
                return Content(e.Message);
            }
        }


        // POST api/<TruPriceController>
        [HttpPost]
        public async Task<ActionResult> PostSell(TruSellOrder order)
        {
            // TruTradingEngine tradingEngine = TruTradingEngine.Instance;
            // TODO: Validate that Seller TRU ID and PubKey are not null..
            //Require fileds.. qty, limit price, sellerId, SellerPubKey, SendTruMsg.
            
            var newLimitSell = new TruSellOrder()
            {
                Qty = order.Qty,
                LeavesQty = order.Qty,
                CumQty = 0,
                LimitPrice = order.LimitPrice,
                CreatedOn = DateTime.Now,
                UpdatedOn = DateTime.Now,
                SendTruMsg = order.SendTruMsg,
                OrderStatus = Enums.OrderStatus.Initiated,
                PaypalTx = order.PaypalTx
            };
            Console.WriteLine("TRU SELL POSTED!");
            Console.WriteLine(newLimitSell.ID);

            //Next Try and execute the Tru Payment message!!!!!

            TruNodeClient truNodeClient = new TruNodeClient();
            
            TruTxDTO truTxDto = JsonConvert.DeserializeObject<TruTxDTO>(order.SendTruMsg);
  
            var txPosted = await truNodeClient.PostTx(truTxDto);
             
            if(txPosted != null) {

                if (txPosted.Verifications[0].SenderBalance >= truTxDto.TruAmount)
                {
                    using (var db = TruExchangeDb.Instance.Db)
                    {
                        newLimitSell.OrderStatus = OrderStatus.Paid; // Truasis has the TRU in escrow!
                        await db.TruSellOrders.AddAsync(newLimitSell);
                        await db.SaveChangesAsync();

                        var processedOrder = await TruTradingEngine.Instance.ProcessNewOrder(newLimitSell.ID, newLimitSell.OrderType);
                        Console.WriteLine("Trading engine processed Order?");
                        Console.WriteLine(processedOrder);
                        return Content(JsonConvert.SerializeObject(processedOrder));
                    }
                }
            }
            
            Console.WriteLine("DARN PAYMENT did not succeed:(");
            Response.StatusCode = 400;
            return Content(null);
             
      
        }



        // PUT api/<TruPriceController>/5
        [HttpPut("{id}")]
        public async Task<ActionResult> Put(int id, [FromBody] dynamic value)
        {
            try
            {
                using (var db = TruExchangeDb.Instance.Db)
                {
                    var offer = await db.TruSellOrders.FirstOrDefaultAsync(q => q.ID == id);
                    if (offer != null)
                    {
                        var json = value.GetRawText();
                        var updateObj = JsonConvert.DeserializeObject<dynamic>(json);

                        if (updateObj.PaypalTx != null) offer.PaypalTx = updateObj.PaypalTx;
                        if (updateObj.OrderStatus != null) offer.OrderStatus = updateObj.OrderStatus;

                        offer.UpdatedOn = DateTime.Now;
                        Console.WriteLine("Updated TRU SELL... ");
                        Console.WriteLine(offer);
                        await db.SaveChangesAsync();
                        Response.StatusCode = 201;

                        return Content("");
                    }
                    else
                    {
                        Response.StatusCode = 400;
                        return Content("No Such Entry");
                    }
                }
            }
            catch (Exception e)
            {
                Response.StatusCode = 400;
                return Content(e.Message);
            }

        }

        // DELETE api/<TruPriceController>/5
        [HttpDelete("{id}")]
        public async Task<ActionResult> Delete(int id)
        {
            try
            {
                using (var db = TruExchangeDb.Instance.Db)
                {
                    var offer = await db.TruSellOrders.FirstOrDefaultAsync(q => q.ID == id);
                    if (offer != null)
                    {
                        if (offer.OrderStatus == OrderStatus.Paid)
                        {
                            double txFee = 5;
                            var returnMoney = offer.CumQty * offer.LimitPrice - txFee;  // Amount the seller did sell..

                            PaypalClient paypalClient = new PaypalClient();
                            var returnMoneyEmail = offer.SellerPayPalEmail;

                            _ = Task.Run(async () =>
                            { //Refund TRU BUY
                                var paypalResponse = await paypalClient.SendDollarsTo(returnMoneyEmail, returnMoney, offer.ID);
                            });

                            // Send tru back to seller.. 

                            // Return unSold TRU:
                            double returnQty = offer.LeavesQty;

                            TruTxDTO paymentMsg = JsonConvert.DeserializeObject<TruTxDTO>(offer.SendTruMsg);
                            //find pub key by id;

                            var success = await TruTradingEngine.Instance.SendTru(returnQty, Program.MasterWallet.ID, Program.MasterWallet.Keys.PubKey);
                        }
                       
                        offer.OrderStatus = OrderStatus.Canceled;
                       
                        await db.SaveChangesAsync();

                        return Content(JsonConvert.SerializeObject(offer));
                    }
                    else
                    {
                        Response.StatusCode = 201;
                        return Content(JsonConvert.SerializeObject(new TruSellOrder() { ID = id }));
                    }
                }
            }
            catch (Exception e)
            {
                Response.StatusCode = 400;
                return Content(e.Message);
            }
        }
 
    }
}
