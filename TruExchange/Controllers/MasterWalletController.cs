﻿using Microsoft.AspNetCore.Mvc;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace TruExchange.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class MasterWalletController : Controller
    {
        // GET: api/<TruPriceController>
        [HttpGet]
        public ActionResult GetWalletInfo()
        {
            try
            {
               
                var masterWalletInfo = new Dictionary<string, string>()
                    {
                        {"walletId", Program.MasterWallet.ID },
                        {"pubKey", Program.MasterWallet.Keys.PubKey }
                    };

                    return Content(JsonConvert.SerializeObject(masterWalletInfo));
                 
            }
            catch (Exception e)
            {
                Response.StatusCode = 400;
                return Content(e.Message);
            }
        }
 
    }
}
