﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Text;
using System.Threading.Tasks;
using TruExchange.Database;
using TruExchange.Models;
using TruExchange.Models.paypal;
using TruV2Shared;

namespace TruExchange
{

    public class PaypalClient
    {
        internal class PayPalConfig
        {
            public string mode { get; set; }
            public string client_id { get; set; }
            public string client_secret { get; set; }
        }

        private HttpClient client = new HttpClient();
        private string _baseURL { get; set; } = "https://api-m.sandbox.paypal.com/";

        private bool isInitialized = false;

        private PayPalConfig _payPalConfig { get; set; }

        private string OAuthToken = "";

        public void Initialize()
        {
            if (!isInitialized)
            {
                isInitialized = true;
                client.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));
                client.DefaultRequestHeaders.TryAddWithoutValidation("Content-Type", "application/json; charset=utf-8");

                _payPalConfig = getConfiguration("sandbox");

            }

        }


        private PayPalConfig getConfiguration(string mode)
        {  //'sandbox' || 'live'

            string secret = "";
            string client = "";

            if (mode == "sandbox")
            {
                client = "AVLPW3gFaQLRcLgbx-xqqgXNLaE1BWfscepRc5KyTmZQFVN8TStk6v49ZJUOcwFG6sYI2z_Z4C3_kXpI";
                secret = "EDCs8f40D4LUTpw_vT6KLcz9fu_zMSUH05MHJxjrndsxdogpqRFWCE684trOcCmcBK7aJwPttoihIc7u";
            }
            else
            {
                var jsonPaypalKeys = FileOperations.ReadToString("paypalkeys.json");
                if (jsonPaypalKeys == "")
                {
                    Console.WriteLine("You need paypal Keys to run the exchange");
                    Environment.Exit(-1);
                }

                var paypalKeysObj = JsonConvert.DeserializeObject<Dictionary<string, string>>(jsonPaypalKeys);

                client = paypalKeysObj["client"];
                secret = paypalKeysObj["secret"];
           }

            var configObj = new PayPalConfig()
            {
                mode = mode,
                client_id = client,
                client_secret = secret
            };


            return configObj;
        }

        private async Task<string> getOAuthToken()
        {
            string endpoint = @"v1/oauth2/token";

            var data = new Dictionary<string, string>()
            {
                { "grant_type", "client_credentials"}
            };

            using (var requestMessage = new HttpRequestMessage(HttpMethod.Post, _baseURL + endpoint))
            {
                var byteArray = Encoding.ASCII.GetBytes($"{_payPalConfig.client_id}:{_payPalConfig.client_secret}");

                requestMessage.Headers.Authorization = new AuthenticationHeaderValue("Basic", Convert.ToBase64String(byteArray));

                requestMessage.Content = new FormUrlEncodedContent(data);

                var response = await client.SendAsync(requestMessage);

                if (response.IsSuccessStatusCode)
                {

                    var res = await response.Content.ReadAsStringAsync();
                    dynamic deserialized = JsonConvert.DeserializeObject(res);
                    try
                    {
                        string oAuthtoken = deserialized.access_token;

                        client.DefaultRequestHeaders.Authorization = new AuthenticationHeaderValue("Bearer", oAuthtoken);

                        return oAuthtoken;
                    }
                    catch (Exception e)
                    {
                        Console.WriteLine(e.Message);
                        Console.WriteLine("Deserialization Exception");
                        return "";
                    }

                }
            }

            return "";

        }


        public async Task<PaypalPayoutResponse> SendDollarsTo(string email, double dollarAmount, int PayoutId)
        {

            OAuthToken = await getOAuthToken();

            string endpoint = @"v1/payments/payouts";
            var payoutBodyContent = new PayoutBody();
            payoutBodyContent.sender_batch_header = new SenderBatchHeader()
            {
                sender_batch_id = Guid.NewGuid().ToString(),
                email_subject = "Payment for TRU Sale Complete",
                email_message = "Thanks for selling your TRU!",
                note = "TRU Crypto Sale",
                recipient_type = "EMAIL"
            };

            payoutBodyContent.items = new List<PayoutItem>(){
                new PayoutItem()
                {
                    receiver = email,
                    amount = new PayoutAmount() { value = Math.Round(dollarAmount, 2).ToString() },
                    note = "You are getting paid for selling TRU",
                    sender_item_id = PayoutId.ToString()
                }
            };

            var json = JsonConvert.SerializeObject(payoutBodyContent);

            var content = new StringContent(json.ToString(), Encoding.UTF8, "application/json");

            var response = await client.PostAsync(_baseURL + endpoint, content);

            if (response.IsSuccessStatusCode)
            {
                var res = await response.Content.ReadAsStringAsync();
                try
                {
                    Console.WriteLine(res);
                    PaypalPayoutResponse resp = JsonConvert.DeserializeObject<PaypalPayoutResponse>(res);
                    return resp;
                }
                catch (Exception e)
                {
                    Console.WriteLine(e.Message);
                    Console.WriteLine("Deserialization Exception");
                    return null;
                }
            }
            return null;
        }


        public async Task<dynamic> GetPayoutStatus(string batchID)
        {
            OAuthToken = await getOAuthToken();

            Console.WriteLine("GET TRU PAYMENT STATUS>>>");
            Console.WriteLine(batchID);
            string endpoint = @"v1/payments/payouts/" + batchID;

            var response = await client.GetAsync(_baseURL + endpoint);

            if (response.IsSuccessStatusCode)
            {
                Console.WriteLine("Response ON getPayoutStatus");
                Console.WriteLine(response);
                var res = await response.Content.ReadAsStringAsync();

                try
                {
                    var resp = JsonConvert.DeserializeObject<dynamic>(res);
                    Console.WriteLine(res);
                    return resp;
                }
                catch (Exception e)
                {
                    return null;
                }
            }
            return null;
        }



        public async Task<OrderResponse> GetPaypalPaymentIntent(double amount, string buyOrderID)
        {
            OAuthToken = await getOAuthToken();

            var checkoutBodyContent = new OrderRequest()
            {
                intent = "CAPTURE",
                application_context = new OrderApplicationContext()
                {
                    brand_name = "TRUASIS",
                    shipping_preference = "NO_SHIPPING",
                    user_action = "PAY_NOW",
                    landing_page = "LOGIN"
                },
                purchase_units = new List<PurchaseUnitRequest>()
                {
                    new PurchaseUnitRequest()
                    {
                        amount=new PaymentAmount()
                        {
                            value= Math.Round(amount,2).ToString()
                        },
                        custom_id=buyOrderID
                    }
                }
            };


            string endpoint = @"v2/checkout/orders";

            var json = JsonConvert.SerializeObject(checkoutBodyContent);

            var content = new StringContent(json.ToString(), Encoding.UTF8, "application/json");

            var response = await client.PostAsync(_baseURL + endpoint, content);

            if (response.IsSuccessStatusCode)
            {
                var res = await response.Content.ReadAsStringAsync();
                try
                {
                    var deserialized = JsonConvert.DeserializeObject<OrderResponse>(res);
                    return deserialized;
                }
                catch (Exception e)
                {
                    Console.WriteLine(e.Message);
                    Console.WriteLine("Deserialization Exception");
                    return null;
                }
            }
            return null;
        }


        public async Task<PaymentResponse> CheckOnPayment(string paymentId)
        {
            OAuthToken = await getOAuthToken();

            string endpoint = @"v2/checkout/orders/" + paymentId;

            var response = await client.GetAsync(_baseURL + endpoint);

            if (response.IsSuccessStatusCode)
            {
                var res = await response.Content.ReadAsStringAsync();
                try
                {
                    var deserialized = JsonConvert.DeserializeObject<PaymentResponse>(res);
                    return deserialized;
                }
                catch (Exception e)
                {
                    Console.WriteLine(e.Message);
                    Console.WriteLine("Deserialization Exception");
                    return null;
                }
            }
            return null;
        }
    }
}
