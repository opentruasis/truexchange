﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Sockets;
using System.Threading;
using System.Threading.Tasks;
using TruExchange.Database;
using TruExchange.Enums;
using TruExchange.Models;
using TruShared;
using TruShared.WebClient;
using TruV2Shared;

namespace TruExchange
{
    public class TruTradingEngine
    {

        public event EventHandler<EventArgs> NewOrderCreated;

        private static TruTradingEngine _instance = null;

        public static TruTradingEngine Instance { 
            get
            {
                if(_instance != null)
                {
                    return _instance;
                }
                else
                {
                    _instance = new TruTradingEngine();
                    return _instance;
                }
            }
        }

        private TruTradingEngine()
        {

        }

        public void Initialize()
        {
            //maybe load and runtime infor from last shutdown?.. not sure if this method is really needed.

        }


        public void Run()
        {
            while (true)
            {
                //Process orders i.e. check paypal transaction statuses..///

               // Console.WriteLine("RUNTIME");
                Thread.Sleep(3000);
            }

        }
 

        public async Task<TruOrder> ProcessNewOrder(int orderId, OrderType newOrderType, bool isDebug = false)
        {
            List<double> listOfNewPricesBasedOnBids = new List<double>();
            Console.WriteLine("Started ORDER PROCESSING: ");
            //ORDER BOOK!
            List<TruSellOrder> Asks = new List<TruSellOrder>();
            List<TruBuyOrder> Bids = new List<TruBuyOrder>();
            using (var db = TruExchangeDb.Instance.Db)
            {
                TruOrder newOrder = null;

                if(newOrderType == OrderType.Buy)
                {
                    newOrder = await db.TruBuyOrders.FindAsync(orderId);
                }

                if(newOrderType == OrderType.Sell)
                {
                    newOrder = await db.TruSellOrders.FindAsync(orderId);
                }

                if (newOrder == null) return null;

                try
                {

                    Asks = db.TruSellOrders.Where(o => o.OrderStatus == OrderStatus.Paid).ToList();
                    Bids = db.TruBuyOrders.Where(o => o.OrderStatus == OrderStatus.Paid).ToList();

                }
                catch (Exception e)
                {

                }

                Console.WriteLine("Asks Count: " + Asks.Count);
                Console.WriteLine("Bids Count: " + Bids.Count);


                if (newOrder.OrderType == OrderType.Buy)
                {
                    Console.WriteLine("Yep is a Buy");
                    bool isAnAsk = Asks.Count > 0;
                    TruSellOrder bestAsk = null;
                    if (isAnAsk)
                    {

                        Asks.Sort((a, b) =>
                        {
                            return a.LimitPrice > b.LimitPrice ? 1 : -1;
                        });

                        bestAsk = Asks[0]; // Minimum LimitPrices
                        int j = 0;
                        while (bestAsk.LimitPrice > newOrder.LimitPrice)
                        {
                            j++;
                            if (j < Asks.Count)
                            {
                                bestAsk = Asks[j];
                            }
                            else
                            {
                                bestAsk = new TruSellOrder() { LimitPrice = 9999999 };
                                break;
                            }
                        }
                    }
                     
 
                    while (isAnAsk &&
                        (newOrder.LimitPrice == 0.0 || newOrder.LimitPrice >= bestAsk.LimitPrice) && 
                        newOrder.LeavesQty > 0)
                    {

                        var bestOrders = Asks.FindAll((o) =>
                        {
                            return o.LimitPrice <= bestAsk.LimitPrice;
                        });
                        double best_price_qty = 0.0;
                        var matchIdList = new List<int>();
                        bestOrders.ForEach((o) =>
                        {
                            matchIdList.Add(o.ID);
                            best_price_qty += o.LeavesQty;
                        });

                        bestOrders.Sort((a, b) =>
                        {
                            return a.LimitPrice < b.LimitPrice ? 1 : -1;
                        });

                        double match_qty = Math.Min(best_price_qty, newOrder.Qty);
                        if (match_qty > 0)
                        {
                            Console.WriteLine("FOUND A MATCH for a seller to buy from:)");

                            // Generate aggressive order trade first
                            newOrder.CumQty += match_qty;
                            newOrder.LeavesQty -= match_qty;
                            try
                            {

                                await db.TruTrades.AddAsync(
                                    new TruTrade()
                                    {
                                        OrderId = newOrder.ID,
                                        MatchIds = JsonConvert.SerializeObject(matchIdList),
                                        Price = bestAsk.LimitPrice,
                                        Qty = match_qty,
                                        OrderType = OrderType.Buy
                                    }
                                );

                                //Business Rule!:
                                //For a BUY order.
                                //Release TRU for all Trades i.e. send match_qty right away!!!

                              
                                if (!isDebug)
                                {
                                    var buyerTruID = (newOrder as TruBuyOrder).BuyerTruID;
                                    var buyerPubKey = (newOrder as TruBuyOrder).BuyerPubKey;
                                    double sendMatchQty = match_qty;
                                    _ = Task.Run(async () =>
                                      {
                                          bool paymentSentSuccessfully = false;
                                          //while (!paymentSentSuccessfully)
                                         // {
                                              paymentSentSuccessfully = await SendTru(sendMatchQty, buyerTruID, buyerPubKey);
                                              Console.WriteLine("Payment Sent Successfully? ");
                                              Console.WriteLine(paymentSentSuccessfully);
                                         // }
                                      });
                                }

                                listOfNewPricesBasedOnBids.Add(newOrder.LimitPrice);
 
                            }
                            catch (Exception e)
                            {

                            }

                        }

                        //Generate the passive executions
                        int i = 0;
                        while (match_qty > 0)
                        {
                            Console.WriteLine("Passive Match Quantity greater than 0 !!!");
                            var hitOrder = bestOrders[i];

                            double order_match_qty = Math.Min(match_qty, hitOrder.LeavesQty);
                            if (order_match_qty > 0)
                            {
                                //Add new trade...
                                try
                                {

                                    await db.TruTrades.AddAsync(
                                        new TruTrade()
                                        {
                                            OrderId = hitOrder.ID,
                                            MatchIds = JsonConvert.SerializeObject(new List<int>() { newOrder.ID }),
                                            Price = bestAsk.LimitPrice,
                                            Qty = order_match_qty,
                                            OrderType = OrderType.Sell
                                        }
                                    );

                                    //Business Rule!:
                                    //For a BUY order.
                                    //Release TRU for all Trades i.e. send match_qty right away!!!

                                    var buyerTruID = (newOrder as TruBuyOrder).BuyerTruID;
                                    var buyerPubKey = (newOrder as TruBuyOrder).BuyerPubKey;
                                    if (!isDebug)
                                    {
                                        double sendMatchQty = order_match_qty;
                                        _ = Task.Run(async () =>
                                        {
                                            bool paymentSentSuccessfully = false;
                                            //while (!paymentSentSuccessfully)
                                            //{
                                                paymentSentSuccessfully = await SendTru(sendMatchQty, buyerTruID, buyerPubKey);
                                           // }
                                        });
                                    }

                                    listOfNewPricesBasedOnBids.Add(newOrder.LimitPrice);
                                  
                                }
                                catch (Exception e)
                                {

                                }

                                hitOrder.CumQty += order_match_qty;
                                hitOrder.LeavesQty -= order_match_qty;
                                match_qty -= order_match_qty;


                                //Hit orders here are SELL TRU orders.. Therefore Only Send Tru of the FUll amount IF
                                // the HitOrder is fullfilled.. This is different from the BUY order. where we send Tru every hit.
                              
                                if (hitOrder.LeavesQty <= 0.00001)
                                {
                                    hitOrder.OrderStatus = OrderStatus.Fullfilled;
                                    
                                    //Sell Order Fullfilled!
                                    //Send $$$ to the seller.
                                    if (!isDebug)
                                    {
                                        double txFee = 5;
                                        PaypalClient paypalClient = new PaypalClient();
                                        var sellerEmail = bestOrders[i].SellerPayPalEmail;
                                        var totalSellingPrice = bestOrders[i].LimitPrice * bestOrders[i].Qty - txFee;
                                        _ = Task.Run(async () =>
                                        {
                                            var paypalResponse = await paypalClient.SendDollarsTo(sellerEmail, totalSellingPrice, bestOrders[i].ID);
                                        });
                                    }

                                    await db.SaveChangesAsync();
                                }
                                // Get only asks that have been paid for. (i.e. the seller has sent TRU to the Escrow Truasis Account)
                                Asks = db.TruSellOrders.Where(o => o.OrderStatus == OrderStatus.Paid).ToList();

                            }
                            i++;
                        }

                        isAnAsk = Asks.Count > 0;
                        if (isAnAsk)
                        {
                            Asks.Sort((a, b) =>
                            {
                                return a.LimitPrice > b.LimitPrice ? 1 : -1;
                            });

                            bestAsk = Asks[0];
                            int j = 0;
                            while (bestAsk.LimitPrice > newOrder.LimitPrice)
                            {
                                j++;
                                if (j < Asks.Count)
                                {
                                    bestAsk = Asks[j];
                                }
                                else
                                {
                                    bestAsk = new TruSellOrder() { LimitPrice = 9999999 };
                                    break;
                                }
                            }
                        }
                    }

                    Console.WriteLine("Finished Pro Rata Algorithm for this new BUY Order:");
                    Console.WriteLine(newOrder.Qty);
                    Console.WriteLine(newOrder.LimitPrice);
                    Console.WriteLine("----");
                    Console.WriteLine(newOrder.LeavesQty);
                    Console.WriteLine(newOrder.CumQty);
                    if(newOrder.LeavesQty <= 0.00001)
                    {
                        Console.WriteLine("it's Fullfilled!");
                        newOrder.OrderStatus = OrderStatus.Fullfilled;
                        await db.SaveChangesAsync();
                    }
                             
                }
                else
                {

                    Console.WriteLine("Yep is a Sell");
                    bool isABid = Bids.Count > 0;
                    TruBuyOrder bestBid = null;
                    if (isABid)
                    {
                        Console.WriteLine("there is potentially a bid to buy this offer");
                        Bids.Sort((a, b) =>
                        {
                            return a.LimitPrice < b.LimitPrice ? 1 : -1;
                        });

                        bestBid = Bids[0]; // Minimum LimitPrices
                        int j = 0;
                        while(bestBid.LimitPrice < newOrder.LimitPrice)
                        {
                            j++;
                            if (j < Bids.Count)
                            {
                                bestBid = Bids[j];
                            }
                            else
                            {
                                bestBid = new TruBuyOrder() { LimitPrice = 0 };
                                break;
                            }
                        }
                    }
                   

                    while (isABid && 
                        (newOrder.LimitPrice == 0.0 || newOrder.LimitPrice <= bestBid.LimitPrice)
                        && newOrder.LeavesQty > 0)
                    {
                        Console.WriteLine("Processing Sell");
                        var bestOrders = Bids.FindAll((o) =>
                        {
                            return o.LimitPrice >= bestBid.LimitPrice;
                        });
                        bestOrders.Sort((a, b) =>
                        {
                            return a.LimitPrice > b.LimitPrice ? 1 : -1;
                        });

                        double best_price_qty = 0;
                        var matchIdList = new List<int>();
                        bestOrders.ForEach((o) =>
                        {
                            matchIdList.Add(o.ID);
                            best_price_qty += o.LeavesQty;
                        });

                        double match_qty = Math.Min(best_price_qty, newOrder.Qty);
                        Console.WriteLine("Match Qty");
                        Console.WriteLine(match_qty);
                        newOrder.CumQty += match_qty;
                        newOrder.LeavesQty -= match_qty;

                        if (match_qty > 0)
                        {
                            Console.WriteLine("FOUND A MATCH of a buyer to sell to buy from:)");
                            try
                            {

                                await db.TruTrades.AddAsync(
                                    new TruTrade()
                                    {
                                        OrderId = newOrder.ID,
                                        MatchIds = JsonConvert.SerializeObject(matchIdList),
                                        Price = bestBid.LimitPrice,
                                        Qty = match_qty,
                                        OrderType = OrderType.Sell
                                    }
                                );
                                Console.WriteLine("Sale matched and Trade saved");
                                //Sale is matched.  Release Cash! -- wait not yet.. ensure that the order is fullfilled!!
                                listOfNewPricesBasedOnBids.Add(bestBid.LimitPrice);
  
                            }
                            catch (Exception e)
                            {

                            }
                        }

                        //Generate the passive executions
                        int i = 0;
                        while (match_qty > 0)
                        {
                            Console.WriteLine("Passive Match Quantity greater than 0 !!!");
                            var hitOrder = bestOrders[i];

                            double order_match_qty = Math.Min(match_qty, hitOrder.LeavesQty);

                            if (order_match_qty > 0)
                            {
                                //Add new trade...
                                try
                                {

                                    await db.TruTrades.AddAsync(
                                        new TruTrade()
                                        {
                                            OrderId = hitOrder.ID,
                                            MatchIds = JsonConvert.SerializeObject(new List<int>() { newOrder.ID }),
                                            Price = hitOrder.LimitPrice,
                                            Qty = order_match_qty,
                                            OrderType = OrderType.Buy
                                        }
                                    );

                                    if (!isDebug)
                                    {
                                        var buyerTruID = (hitOrder as TruBuyOrder).BuyerTruID;
                                        var buyerPubKey = (hitOrder as TruBuyOrder).BuyerPubKey;

                                        double sendMatchQty = order_match_qty;
                                        _ = Task.Run(async () =>
                                        {
                                            bool paymentSentSuccessfully = false;
                                            //while (!paymentSentSuccessfully)
                                            //{
                                                paymentSentSuccessfully = await SendTru(sendMatchQty, buyerTruID, buyerPubKey);
                                                Console.WriteLine("Payment Sent Successfully? ");
                                                Console.WriteLine(paymentSentSuccessfully);
                                           // }
                                        });
                                    }

                                    listOfNewPricesBasedOnBids.Add(hitOrder.LimitPrice);
                                 

                                    //Process Trade!1

                                }
                                catch (Exception e)
                                {

                                }

                                hitOrder.CumQty += order_match_qty;
                                hitOrder.LeavesQty -= order_match_qty;
                                match_qty -= order_match_qty;

                                //  Hit order here is a BUY TRU order.. therefore I need to send TRU of the amount order_matched_qty.
 
                                if (hitOrder.LeavesQty <= 0.00001)
                                {
                                    hitOrder.OrderStatus = OrderStatus.Fullfilled;
                                    await db.SaveChangesAsync();
                                }
                                Bids = db.TruBuyOrders.Where(o => o.OrderStatus == OrderStatus.Paid).ToList();

                            }
                            i++;
                        }

                        isABid = Bids.Count > 0;
                        if (isABid)
                        {
                            Bids.Sort((a, b) =>
                            {
                                return a.LimitPrice < b.LimitPrice ? 1 : -1;  //Max Limit Price
                            });

                            bestBid = Bids[0]; // Minimum LimitPrices
                            int j = 0;
                            while (bestBid.LimitPrice < newOrder.LimitPrice)
                            {
                                j++;
                                if (j < Bids.Count)
                                {
                                    bestBid = Bids[j];
                                }
                                else
                                {
                                    bestBid = new TruBuyOrder() { LimitPrice = 0 };
                                    break;
                                }
                            }
                        }
                    }

                    Console.WriteLine("Finished Pro Rata Algorithm for this new SELL Order:");
                    Console.WriteLine(newOrder.Qty);
                    Console.WriteLine(newOrder.LimitPrice);
                    Console.WriteLine("----");
                    Console.WriteLine(newOrder.LeavesQty);
                    Console.WriteLine(newOrder.CumQty);

                    if (newOrder.LeavesQty <= 0.00001)
                    {
                        Console.WriteLine("IT is fullfilled!! now Send out Paypal Payment");
                        newOrder.OrderStatus = OrderStatus.Fullfilled;

                        // Send Payment.. Order fullfiled instantly!
                        // Send Paypal $$ to seller of TRU since the TRU was matched with BUYERS.
                        if (!isDebug)
                        {
                            double txFee = 5;
                            PaypalClient paypalClient = new PaypalClient();
                            var sellerEmail = (newOrder as TruSellOrder).SellerPayPalEmail;
                            var totalSellingPrice = newOrder.LimitPrice * newOrder.Qty - txFee;

                            _ = Task.Run(async () =>
                            {
                                Console.WriteLine("Sending payment to the seller");
                                var paypalResponse = await paypalClient.SendDollarsTo(sellerEmail, totalSellingPrice, newOrder.ID);
                                Console.WriteLine(paypalResponse);
                            });
                        }
                        Console.WriteLine("Save Sale");
                        await db.SaveChangesAsync();
                    }

                
                }
               
                if (listOfNewPricesBasedOnBids.Count > 0)
                {
                    var bestPrice = listOfNewPricesBasedOnBids.Max<double>();
                    await db.TruPrices.AddAsync(new TruPrice()
                    {
                        TimeStamp = DateTime.Now,
                        Price = bestPrice
                    });
                }
               
                
                await db.SaveChangesAsync();
                return newOrder;
            }

            
        }

        public async Task<bool> SendTru(double sendQty, string payeeTruID, string payeePubKey)
        {
            //Next Try and execute the Tru Payment message!!!!!

            TruNodeClient truNodeClient = new TruNodeClient();
             
            TruTx newTx = new TruTx();
            newTx.ReceiverId = payeeTruID;
            newTx.ReceiverPubKey = payeePubKey;
            newTx.TruAmount = sendQty;
            newTx.SignerId = Program.MasterWallet.ID;

            TruRSACrypto.SignMsg(newTx, Program.MasterWallet.Keys.PrivKey);
              
            TruTxDTO newTxDto = new TruTxDTO();
            newTxDto.ID = newTx.ID;
            newTxDto.ReceiverId = payeeTruID;
            newTxDto.ReceiverPubKey = payeePubKey;
            newTxDto.TruAmount = sendQty;
            newTxDto.Signature = newTx.Signature;
            newTxDto.SignerId = Program.MasterWallet.ID;

            var success = await truNodeClient.PostTx(newTxDto);
             
            if (success != null) { 
                Console.WriteLine("Payment did succeeed");
                return true;
            }
            else
            {
                Console.WriteLine("DARN PAYMENT did not succeed:(");
                return false;
            }
           
        }
    }
}