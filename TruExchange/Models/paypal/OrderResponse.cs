﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace TruExchange.Models.paypal
{
    public class OrderResponse
    {
        public string id { get; set; }
        public string status { get; set; }
        public List<LinkDescription> links { get; set; }
    }
}
