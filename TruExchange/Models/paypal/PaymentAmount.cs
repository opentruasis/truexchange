﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace TruExchange.Models.paypal
{
    public class PaymentAmount
    {
        public string currency_code { get; set; } = "USD";
        public string value { get; set; }
    }
}
