﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace TruExchange.Models.paypal
{
    public class PaypalName
    {
        public string prefix { get; set; }
        public string given_name { get; set; }
        public string surname { get; set; }
        public string middle_name { get; set; }
        public string suffix { get; set; }
        public string alternate_full_name { get; set; }
        public string full_name { get; set; }
    }
}
