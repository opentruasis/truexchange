﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace TruExchange.Models.paypal
{
    public class OrderRequest
    {
        public string intent { get; set; } = "CAPTURE";

        public OrderApplicationContext application_context { get; set; }

        public List<PurchaseUnitRequest> purchase_units { get; set; }
 
    }
}
