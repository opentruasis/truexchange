﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace TruExchange.Models.paypal
{
    public class PaymentResponse
    {
        public string id { get; set; }
        public string create_time { get; set; }
        public string update_time { get; set; }
        public dynamic payment_source { get; set; }
        public string intent { get; set; }
        public PaypalPayer payer { get; set; }
        public string status { get; set; }
        public List<LinkDescription> links { get; set; }
    }
}
