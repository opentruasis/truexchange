﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace TruExchange.Models.paypal
{
    public class PurchaseUnitRequest
    {
        public PaymentAmount amount { get; set; }
        public string custom_id { get; set; }
    }
}
