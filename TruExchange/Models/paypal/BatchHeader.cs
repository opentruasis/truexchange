﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace TruExchange.Models.paypal
{
    public class BatchHeader
    {
        public string payout_batch_id { get; set; }
        public string batch_status { get; set; }
        public string time_created { get; set; }
        public SenderBatchHeader sender_batch_header { get; set; }
        public PaypalError errors { get; set; }
    }
}
