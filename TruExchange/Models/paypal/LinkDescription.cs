﻿namespace TruExchange.Models.paypal
{
    public class LinkDescription
    {
        public string href{get; set;}
        public string rel { get; set; }
        public string method {get; set;}
    }
}