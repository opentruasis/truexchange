﻿namespace TruExchange.Models.paypal
{
    public class ErrorDetails
    {
        public string field { get; set; }
        public string value {get; set;}
        public string    location { get; set; }
        public string  issue { get; set; }
    }
}