﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace TruExchange.Models.paypal
{
    public class PaypalPayoutResponse
    {
        public BatchHeader batch_header { get; set; }
        public List<LinkDescription> links { get; set; }
    }
}
