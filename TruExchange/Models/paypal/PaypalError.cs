﻿using System.Collections.Generic;

namespace TruExchange.Models.paypal
{
    public class PaypalError
    {
        public string name { get; set; }
        public string message { get; set; }
        public string debug_id { get; set; }
        public string information_link { get; set; }
        public List<ErrorDetails> details { get; set; }

        public List<LinkDescription> links { get; set; }

    }
}