﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace TruExchange.Models.paypal
{
    public class PayoutBody

    {
        public SenderBatchHeader sender_batch_header { get; set; } = new SenderBatchHeader();

        public List<PayoutItem> items { get; set; } = new List<PayoutItem>();
    }
}
