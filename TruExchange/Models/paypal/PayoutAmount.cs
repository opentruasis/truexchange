﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace TruExchange.Models.paypal
{
    public class PayoutAmount
    {
        public string currency  { get; set; } = "USD";
        public string value { get; set; }
        
    }
}
