﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace TruExchange.Models.paypal
{
    public class OrderApplicationContext
    {
        public string brand_name { get; set; } = "TRUASIS";
        public string landing_page { get; set; } = "LOGIN";
        public string shipping_preference { get; set; } = "NO_SHIPPING";
        public string user_action { get; set; }  = "PAY_NOW";
 
#if (DEBUG)
        public string return_url { get; set; } = "http://localhost:5000/paypalbuyordercomplete";
#elif (RELEASE)
        public string return_url { get; set; } = "https://ex.truasis.com/paypalbuyordercomplete";
#endif


        //"landing_page": "LOGIN",
        //"shipping_preference": "NO_SHIPPING",
        //"user_action": "PAY_NOW",
        //"return_url":"https://truasis.com/trupurchasecomplete"

    }
}
