﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace TruExchange.Models.paypal
{
    public class SenderBatchHeader
    {
        public string sender_batch_id { get; set; }
        public string recipient_type { get; set; } = "EMAIL";
        public string email_subject  { get; set; }
        public string email_message { get; set; }
        public string note { get; set; }
    }
}
