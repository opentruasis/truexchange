﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace TruExchange.Models.paypal
{
    public class PayoutItem
    {
        public string recipient_type { get; set; } = "EMAIL";

        public PayoutAmount amount {get; set;}

        public string note { get; set; }

        public string receiver { get; set; }

        public string sender_item_id { get; set; }
 
    }
}
