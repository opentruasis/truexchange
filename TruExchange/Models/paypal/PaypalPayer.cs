﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace TruExchange.Models.paypal
{
    public class PaypalPayer
    {
        public string email_address { get; set; } //paypal email
        public string payer_id { get; set; } //paypal id
    }
}
