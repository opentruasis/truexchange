using Microsoft.AspNetCore.Hosting;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.Hosting;
using Microsoft.Extensions.Logging;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using TruV2Shared;

namespace TruExchange
{
    public class Program
    {

        public static TruWallet MasterWallet = null;

        public static void Main(string[] args)
        {
           
                //Load the Master Wallet
            var jsonTruasisWallet = FileOperations.ReadToString("SuperWallet.json");
            if(jsonTruasisWallet == "")
            {
                Console.WriteLine("To run Exchange you must have a SuperNode.json file in the working directory");
                Environment.Exit(-1);
            }
            
            MasterWallet = JsonConvert.DeserializeObject<TruWallet>(jsonTruasisWallet);
            
            TruTradingEngine truTradingEngine = TruTradingEngine.Instance;
            truTradingEngine.Initialize();

            Thread runtimeThread = new Thread(new ThreadStart(truTradingEngine.Run));
            runtimeThread.Start();

            CreateHostBuilder(args).Build().Run();
        }

        public static IHostBuilder CreateHostBuilder(string[] args) =>
            Host.CreateDefaultBuilder(args)
                .ConfigureWebHostDefaults(webBuilder =>
                {
                    webBuilder.UseUrls("http://localhost:5000");
                    webBuilder.UseStartup<Startup>();
                });
    }
}
