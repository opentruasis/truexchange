﻿using Microsoft.EntityFrameworkCore;
using TruExchange.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace TruExchange.Database
{
    public class SQLiteDBContext : DbContext
    {
        public DbSet<TruPrice> TruPrices { get; set; }
        public DbSet<TruBuyOrder> TruBuyOrders { get; set; }
        public DbSet<TruSellOrder> TruSellOrders { get; set; }
        public DbSet<TruTrade> TruTrades { get; set; }
   
        protected override void OnConfiguring(DbContextOptionsBuilder options)
                => options.UseSqlite("Data Source=truexchange.db");
         
    }
}
