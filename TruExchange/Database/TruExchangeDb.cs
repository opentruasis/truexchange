﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace TruExchange.Database
{
    public class TruExchangeDb
    {
        public SQLiteDBContext Db
        {
            get
            {
                return new SQLiteDBContext();
            }
        }

        private static TruExchangeDb _instance;

        public static TruExchangeDb Instance
        {
            get
            {
                if (_instance != null)
                {
                    return _instance;
                }
                else
                {
                    _instance = new TruExchangeDb();
                    return _instance;
                }
            }
        }

        private TruExchangeDb(){
        }

    }
}
