﻿using System;
using Microsoft.EntityFrameworkCore.Migrations;

namespace TruExchange.Migrations
{
    public partial class init : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.CreateTable(
                name: "TruBuyOrders",
                columns: table => new
                {
                    ID = table.Column<int>(type: "INTEGER", nullable: false)
                        .Annotation("Sqlite:Autoincrement", true),
                    BuyerTruID = table.Column<string>(type: "TEXT", nullable: true),
                    BuyerPubKey = table.Column<string>(type: "TEXT", nullable: true),
                    PaypalBuyURL = table.Column<string>(type: "TEXT", nullable: true),
                    PaypalTx = table.Column<string>(type: "TEXT", nullable: true),
                    PaypalBuyerEmail = table.Column<string>(type: "TEXT", nullable: true),
                    CreatedOn = table.Column<DateTime>(type: "TEXT", nullable: false),
                    UpdatedOn = table.Column<DateTime>(type: "TEXT", nullable: false),
                    Qty = table.Column<double>(type: "REAL", nullable: false),
                    CumQty = table.Column<double>(type: "REAL", nullable: false),
                    LeavesQty = table.Column<double>(type: "REAL", nullable: false),
                    LimitPrice = table.Column<double>(type: "REAL", nullable: false),
                    OrderStatus = table.Column<byte>(type: "INTEGER", nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_TruBuyOrders", x => x.ID);
                });

            migrationBuilder.CreateTable(
                name: "TruPrices",
                columns: table => new
                {
                    ID = table.Column<int>(type: "INTEGER", nullable: false)
                        .Annotation("Sqlite:Autoincrement", true),
                    TimeStamp = table.Column<DateTime>(type: "TEXT", nullable: false),
                    Price = table.Column<double>(type: "REAL", nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_TruPrices", x => x.ID);
                });

            migrationBuilder.CreateTable(
                name: "TruSellOrders",
                columns: table => new
                {
                    ID = table.Column<int>(type: "INTEGER", nullable: false)
                        .Annotation("Sqlite:Autoincrement", true),
                    SellerPayPalEmail = table.Column<string>(type: "TEXT", nullable: true),
                    PaypalTx = table.Column<string>(type: "TEXT", nullable: true),
                    SendTruMsg = table.Column<string>(type: "TEXT", nullable: true),
                    CreatedOn = table.Column<DateTime>(type: "TEXT", nullable: false),
                    UpdatedOn = table.Column<DateTime>(type: "TEXT", nullable: false),
                    Qty = table.Column<double>(type: "REAL", nullable: false),
                    CumQty = table.Column<double>(type: "REAL", nullable: false),
                    LeavesQty = table.Column<double>(type: "REAL", nullable: false),
                    LimitPrice = table.Column<double>(type: "REAL", nullable: false),
                    OrderStatus = table.Column<byte>(type: "INTEGER", nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_TruSellOrders", x => x.ID);
                });

            migrationBuilder.CreateTable(
                name: "TruTrades",
                columns: table => new
                {
                    ID = table.Column<int>(type: "INTEGER", nullable: false)
                        .Annotation("Sqlite:Autoincrement", true),
                    TimeStamp = table.Column<DateTime>(type: "TEXT", nullable: false),
                    OrderId = table.Column<int>(type: "INTEGER", nullable: false),
                    MatchIds = table.Column<string>(type: "TEXT", nullable: true),
                    Price = table.Column<double>(type: "REAL", nullable: false),
                    Qty = table.Column<double>(type: "REAL", nullable: false),
                    OrderType = table.Column<byte>(type: "INTEGER", nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_TruTrades", x => x.ID);
                });
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropTable(
                name: "TruBuyOrders");

            migrationBuilder.DropTable(
                name: "TruPrices");

            migrationBuilder.DropTable(
                name: "TruSellOrders");

            migrationBuilder.DropTable(
                name: "TruTrades");
        }
    }
}
