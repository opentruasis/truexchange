﻿using Microsoft.EntityFrameworkCore;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using TruExchange;
using TruExchange.Database;
using TruExchange.Models;

namespace ExchangeTests
{
    [TestClass]
    public class CornerCaseTrades
    {

        private async Task ClearTables()
        {
            using (var db = TruExchangeDb.Instance.Db)
            {
                await db.Database.ExecuteSqlRawAsync("DELETE FROM TruBuyOrders");
                await db.Database.ExecuteSqlRawAsync("DELETE FROM TruSellOrders");
                await db.Database.ExecuteSqlRawAsync("DELETE FROM TruTrades");
                await db.Database.ExecuteSqlRawAsync("DELETE FROM TruPrices");
                await db.SaveChangesAsync();
            }
        }

        private async Task<TruBuyOrder> CreateMockBuyOrder(double qty, double limitPrice)
        {

            TruBuyOrder buyOrder = new TruBuyOrder()
            {
                Qty = qty,
                LeavesQty = qty,
                CumQty = 0,
                LimitPrice = limitPrice
            };

            buyOrder.OrderStatus = TruExchange.Enums.OrderStatus.Paid;
            try
            {
                using (var db = TruExchangeDb.Instance.Db)
                {
                    await db.TruBuyOrders.AddAsync(buyOrder);
                    await db.SaveChangesAsync();
                }
            }
            catch (Exception e)
            {
                Console.WriteLine(e.Message);
            }
            return buyOrder;
        }


        private async Task<TruSellOrder> CreateMockSellOrder(double qty, double limitPrice)
        {

            TruSellOrder sellOrder = new TruSellOrder()
            {
                Qty = qty,
                LeavesQty = qty,
                CumQty = 0,
                LimitPrice = limitPrice
            };

            sellOrder.OrderStatus = TruExchange.Enums.OrderStatus.Paid;
            try
            {
                using (var db = TruExchangeDb.Instance.Db)
                {
                    await db.TruSellOrders.AddAsync(sellOrder);
                    await db.SaveChangesAsync();
                }
            }
            catch (Exception e)
            {
                Console.WriteLine(e.Message);
            }
            return sellOrder;
        }




        [TestMethod]
        public async Task CornerCase1()
        {
            //Create Buy Order save in db..
            await ClearTables();

            var buyOrder = await CreateMockBuyOrder(1000, 1.2);
            await CreateMockSellOrder(1000, 1.1);

            var processedOrder = await TruTradingEngine.Instance.ProcessNewOrder(buyOrder.ID, buyOrder.OrderType, true);

        }


        [TestMethod]
        public async Task CornerCase2()
        {
            //Create Buy Order save in db..
            await ClearTables();

            await CreateMockBuyOrder(1000, 1.2);
            var sellOrder = await CreateMockSellOrder(1000, 1.1);

            var processedOrder = await TruTradingEngine.Instance.ProcessNewOrder(sellOrder.ID, sellOrder.OrderType, true);

        }

        [TestMethod]
        public async Task CornerCase3()
        {
            //Create Buy Order save in db..
            await ClearTables();

            await CreateMockBuyOrder(2000, 1.2);
            var sellOrder = await CreateMockSellOrder(1000, 1.5);

            var processedOrder = await TruTradingEngine.Instance.ProcessNewOrder(sellOrder.ID, sellOrder.OrderType, true);

            var sellOrder2 = await CreateMockSellOrder(5000, 1.1);
            processedOrder = await TruTradingEngine.Instance.ProcessNewOrder(sellOrder2.ID, sellOrder2.OrderType, true);

        }


        [TestMethod]
        public async Task CornerCase4()
        {
            //Create Buy Order save in db..
            await ClearTables();

            await CreateMockBuyOrder(2000, 1.2);
            var sellOrder = await CreateMockSellOrder(10.1, 1.5);

            var processedOrder = await TruTradingEngine.Instance.ProcessNewOrder(sellOrder.ID, sellOrder.OrderType, true);

            var sellOrder2 = await CreateMockSellOrder(5000, 1.1);
            processedOrder = await TruTradingEngine.Instance.ProcessNewOrder(sellOrder2.ID, sellOrder2.OrderType, true);


            var buyOrder = await CreateMockBuyOrder(200, 2);
            processedOrder = await TruTradingEngine.Instance.ProcessNewOrder(buyOrder.ID, buyOrder.OrderType, true);

            var buyOrder2 = await CreateMockBuyOrder(10000, 50);
            processedOrder = await TruTradingEngine.Instance.ProcessNewOrder(buyOrder2.ID, buyOrder2.OrderType, true);

        }



        [TestMethod]
        public async Task CornerCase5()
        {
            //Create Buy Order save in db..
            await ClearTables();

            await CreateMockBuyOrder(10.5, 1.2);
            var sellOrder = await CreateMockSellOrder(10.1, 1.1);

            var processedOrder = await TruTradingEngine.Instance.ProcessNewOrder(sellOrder.ID, sellOrder.OrderType, true);

        }

        [TestMethod]
        public async Task CornerCase6()
        {
            //Create Buy Order save in db..
            await ClearTables();

            await CreateMockBuyOrder(100, 0.5);
            await CreateMockBuyOrder(100, 0.25);
            await CreateMockBuyOrder(100, 5);
            await CreateMockBuyOrder(100, 50);

            var sellOrder = await CreateMockSellOrder(1000, 0.6);

            var processedOrder = await TruTradingEngine.Instance.ProcessNewOrder(sellOrder.ID, sellOrder.OrderType, true);

            sellOrder = await CreateMockSellOrder(1000, 0.61);

            processedOrder = await TruTradingEngine.Instance.ProcessNewOrder(sellOrder.ID, sellOrder.OrderType, true);

            var newBuy = await CreateMockBuyOrder(100, 50);

            processedOrder = await TruTradingEngine.Instance.ProcessNewOrder(newBuy.ID, newBuy.OrderType, true);


        }


        [TestMethod]
        public async Task CornerCase7()
        {
            //Create Buy Order save in db..
            await ClearTables();

            await CreateMockSellOrder(10, 0.5);
            await CreateMockSellOrder(100, 0.25);
            await CreateMockSellOrder(100, 5);
            await CreateMockSellOrder(100, 50);

            var buyOrder = await CreateMockBuyOrder(1000, 0.6);

            var processedOrder = await TruTradingEngine.Instance.ProcessNewOrder(buyOrder.ID, buyOrder.OrderType, true);

            var sellOrder = await CreateMockSellOrder(100, 0.5);
          
            processedOrder = await TruTradingEngine.Instance.ProcessNewOrder(sellOrder.ID, sellOrder.OrderType, true);

     
            buyOrder = await CreateMockBuyOrder(1000, 5.1);
          
            processedOrder = await TruTradingEngine.Instance.ProcessNewOrder(buyOrder.ID, buyOrder.OrderType, true);

            sellOrder = await CreateMockSellOrder(100, 0.5);

            processedOrder = await TruTradingEngine.Instance.ProcessNewOrder(sellOrder.ID, sellOrder.OrderType, true);

            buyOrder = await CreateMockBuyOrder(1000, 5.1);

            processedOrder = await TruTradingEngine.Instance.ProcessNewOrder(buyOrder.ID, buyOrder.OrderType, true);

            buyOrder = await CreateMockBuyOrder(1000, 5.1);

            processedOrder = await TruTradingEngine.Instance.ProcessNewOrder(buyOrder.ID, buyOrder.OrderType, true);

            sellOrder = await CreateMockSellOrder(10000, 0.5);

            processedOrder = await TruTradingEngine.Instance.ProcessNewOrder(sellOrder.ID, sellOrder.OrderType, true);


        }
    }
}
