﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using TruExchange;
using TruExchange.Database;
using TruExchange.Enums;
using TruExchange.Models;

namespace ExchangeTests
{
    [TestClass]
    public class PaypalTests
    {

        private PaypalClient client;
        private void initializePayPalClient()
        {
            client = new PaypalClient();
            client.Initialize();
        }

        [TestMethod]
        public async Task SendMoney()
        {
           
        }





        [TestMethod]
        public async Task GetBatchStatus()
        {

            initializePayPalClient();
            //try
            //{
            //    using (var db = TruExchangeDb.Instance.Db)
            //    {
            //        var payouts = db.PaypalPayouts.OrderBy(b => b.ID).ToList();

            //        foreach(var p in payouts)
            //        {
            //            if (p.PayPalTX != null)
            //            {
            //                var result = await client.GetPayoutStatus(p.PayPalTX);
            //                Console.WriteLine(result);
            //            }
            //        }
            //    }
            //}
            //catch (Exception e)
            //{
            //    Assert.AreEqual(0, 1);
            //}

            Assert.AreEqual(1, 1);
        }

        [TestMethod]
        public async Task GetPaymentIntent()
        {
            var newLimitBuy = new TruBuyOrder()
            {
                Qty = 10.0,
                LeavesQty = 10.0,
                CumQty = 0,
                LimitPrice = 0.1,
                CreatedOn = DateTime.Now,
                UpdatedOn = DateTime.Now,
                OrderStatus = OrderStatus.Initiated
            };

            //try
            //{
            //   await tradingEngine.ProcessNewOrder(newLimitBuy);
            //}catch(Exception e)
            //{
            //    Console.WriteLine("ProcessNewOrder Error");
            //    Console.WriteLine(e.Message);
            //}

            using (var db = TruExchangeDb.Instance.Db)
            {
                await db.TruBuyOrders.AddAsync(newLimitBuy);
                await db.SaveChangesAsync();

                double fee = 2.0;
                var buyPrice = newLimitBuy.Qty * newLimitBuy.LimitPrice + fee;
                PaypalClient paypalClient = new PaypalClient();
                paypalClient.Initialize();
                var result = await paypalClient.GetPaypalPaymentIntent(buyPrice, newLimitBuy.ID.ToString());

                if (result != null)
                {
                    newLimitBuy.PaypalBuyURL = result.links[0].href;

                    await db.SaveChangesAsync();


                }
                else
                {
                    Assert.AreEqual(1, 0);
                }



            }
            Assert.AreEqual(1, 1);
            Assert.AreEqual(1, 1);
            
        }
    }
}


